function addProperty () {
    document.getElementById("button-add").classList.add("border-style");
}

function removeProperty () {
    document.getElementById("button-remove").classList.remove("border-style");
}

function toggleProperty () {
    document.getElementById("button-toggle").classList.toggle("border-style");
}